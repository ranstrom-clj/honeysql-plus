(ns honeysql-plus.core
  (:require [clojure.string :as string]
            [com.rpl.specter :refer [defprotocolpath extend-protocolpath ALL MAP-VALS transform
                                     stay-then-continue]]
            [honeysql.core :as sql :refer [call]]
            [honeysql.helpers :as hh]
            [honeysql.format :as hf])
  (:import [honeysql.format Value]
           [honeysql.types SqlCall SqlInline SqlRaw]
           (clojure.lang ArraySeq)))

(defprotocolpath SQLWalker [])

(def sql-walker (stay-then-continue SQLWalker))

(extend-protocolpath
 SQLWalker
 nil nil
 Object nil
 SqlCall [:args ALL sql-walker]
 SqlInline [#(coll? (:value %)) :value ALL sql-walker]
 clojure.lang.PersistentArrayMap [MAP-VALS sql-walker]
 clojure.lang.PersistentHashMap [MAP-VALS sql-walker]
 clojure.lang.PersistentVector [ALL sql-walker]
 clojure.lang.PersistentList [ALL sql-walker]
 clojure.lang.ArraySeq [ALL sql-walker]
 clojure.lang.LazySeq [ALL sql-walker])

(def ^:dynamic *top-level* false)
(def ^:dynamic *clause* nil)

(defn invalid-data [msg e]
  (ex-info msg {:invalid-data e}))

(def not-nil? (complement nil?))

(def handled-sql-types #{SqlCall SqlInline SqlRaw Value})

(defn not-sql-type? [v]
  (not-any? handled-sql-types [(type v)]))

(def sql-type? (complement not-sql-type?))

(defn- vector-or-list? [v] (or (vector? v) (list? v) (= (type v) ArraySeq)))

(defn- remove-map-nils [m]
  (into {} (filter second m)))

(defn map-no-sql-type? [v]
  (and (map? v) (not-sql-type? v)))

(defn apply-sql-map-fn [sql-map f]
  (->> sql-map
       (map
        (fn [[k v]]
          (if (and (some #{:select :columns :from :join :left-join :right-join :where :having :insert-into} [k])
                   (not-nil? v))
            (binding [*clause* k *top-level* true]
              {k (f v)})
            {k v})))
       (into {})))

(def unmentioned-hh-fns (set ["exists"]))

(def honey-functions
  (->> (concat (filter #(not (.contains (str %) " ")) hf/infix-fns)
               (keys (methods hf/fn-handler))
               unmentioned-hh-fns)
       (map keyword)
       (filter #(not= :default %)) set))

(defn honey-function? [v] (and (keyword? v) (some honey-functions [v])))

(def not-honey-function (complement honey-function?))

(defn sql-apply
  ([m f coll]
   (if coll (apply f m coll) m))
  ([m f]
   (apply f m)))

(defn- sql-func? [v]
  (when (or (string? v) (keyword? v))
    (re-matches #"^%[a-zA-Z][a-zA-Z0-9_^.]*" (name v))))

(def not-sql-func? (complement sql-func?))

(defn sql-format [sql] (sql/format sql))

(defn sql-format-quoting [sql {:keys [quoting]}]
  (sql/format sql :quoting quoting))

(defn- quote-field-when
  [field {:keys [quoting quote-when-fn]}]
  (if (or (nil? quote-when-fn) (quote-when-fn field))
    (-> (keyword field) (sql-format-quoting {:quoting quoting}) first name)
    field))

(defn sql-format-quoting-field
  "Takes input field and if keyword, including aliased (e.g. :dbo.table) will return a string with relevant parts quoted.

   If no parts require quoting or the input is not of keyword type, the field will be returned as is.
   "
  [field {:keys [quote-when-fn] :as opts}]
  (if (keyword? field)
    (let [[col tn-alias & err] (reverse (string/split (name field) #"\."))
          fields               (filter not-nil? [tn-alias col])]
      (when err (throw (invalid-data "Field identifier for sql-format-quoting field can only have two parts: " field)))
      (if (or (nil? quote-when-fn) (some quote-when-fn fields))
        (->> fields
             (map #(quote-field-when % opts))
             (string/join "."))
        field))
    field))

(def regex-sql-func #"^%[a-zA-Z][a-zA-Z0-9_^.]*")
(def regex-sql-func-processed #"^%[a-zA-Z0-9._]*")
(def regex-sql-func-aliased #"^%%[a-zA-Z0-9._]*")

(defn- is-function-check? [v rx]
  (and (keyword? v) (re-matches rx (name v))))

(defn sql-function? [v] (is-function-check? v regex-sql-func))
(defn sql-function-processed? [v] (is-function-check? v regex-sql-func-processed))
(defn sql-function-aliased? [v] (is-function-check? v regex-sql-func-aliased))

(defn split-chained-function
  "Separates chained functions into functions and fields."
  [v]
  (when (keyword? v)
    (let [fs (map keyword (string/split (name v) #"\."))
          [fs-split fields] (split-with sql-function? fs)
          field (when (not-empty fields) (->> fields (map name) (string/join ".") keyword))]
      [(vec fs-split) field])))

(defn parse-sql-function [v]
  (cond
    (sql-function-aliased? v) [(-> v name (subs 1) keyword vector)]
    (keyword? v) (split-chained-function v)))

(defn- nest-vector [xs body]
  (if (next xs)
    [(first xs) (nest-vector (next xs) body)]
    (vec (concat xs body))))

(defn apply-custom-function-logic [input-sql]
  (vec
   (map
    (fn [v]
      (let [[fns field] (parse-sql-function (if (vector-or-list? v) (first v) v))]
        (cond
          (and (not-empty fns) (vector-or-list? v)
               *top-level* (= :select *clause*) (= (count v) 2) (not (vector-or-list? (last v))))
          [(nest-vector fns (when field [field])) (last v)]
          (and (not-empty fns) (vector-or-list? v))
          (nest-vector
           fns (concat (when field [field]) (binding [*top-level* false] (apply-custom-function-logic (rest v)))))
          (not-empty fns)
          (nest-vector fns (when field [field]))
          (vector-or-list? v) (binding [*top-level* false] (apply-custom-function-logic v))
          (map-no-sql-type? v) (binding [*top-level* false] (apply-sql-map-fn v apply-custom-function-logic))
          :else v)))
    input-sql)))

(def vector-sql-fn? #(and (vector-or-list? %) (sql-function-processed? (first %))))

(defn apply-sql-call-to-all-functions [input-sql]
  (transform
   [SQLWalker vector-sql-fn?]
   #(apply call (assoc % 0 (-> % vector-sql-fn? name (subs 1) keyword)))
   input-sql))

(defn- add-sql-inline? [v]
  (and (not-any? #(% v) [string? keyword? coll? nil?]) (not-sql-type? v)))

(defn apply-sql-inline [input-sql]
  (transform [SQLWalker add-sql-inline?] sql/inline input-sql))

(defn alias-table
  [v]
  (cond
    (coll? v) v
    (keyword? v) (vector v (-> v name (string/split #"\.") peek keyword))
    :else (throw (invalid-data "Invalid FROM/JOIN" (str "FROM/JOIN value invalid" v)))))

(defn map-every-nth [f n coll]
  (vec (map-indexed #(if (zero? (mod %1 n)) (f %2) %2) coll)))

;(defn map-every-nth
;  ([f n coll]
;   (map-every-nth f n 0 coll))
;  ([f n offset coll]
;   (vec (map-indexed #(if (and (>= %1 offset) (zero? (mod (- %1 offset) n))) (f %2) %2) coll))))

(defn rename-aliased-table [v f]
  (update-in v [0] #(-> % name f string/lower-case keyword)))

;(defn- get-table
;  [v]
;  (cond
;    (coll? v) (first v)
;    (keyword? v) v
;    :else (throw (e/invalid-data "Invalid FROM/JOIN" (str "FROM/JOIN value invalid" v)))))

;(defn from->tables [from]
;  (->> from (map #(get-aliased-table %)) vec))
;
;(defn join->tables [join]
;  (->> join
;       (take-nth 2)
;       (map #(get-aliased-table %))
;       vec))

(defn- keyword->quoted-raw [field opts]
  (if-let [quoted-field (and (every? #(% field) [keyword? not-sql-func? not-honey-function #(not= % :else)])
                             (sql-format-quoting-field field opts))]
    (if (string? quoted-field) (sql/raw quoted-field) quoted-field)
    field))

(defn apply-field-quoting [input-sql opts]
  (->> input-sql
       (map
        (fn [v]
          (cond
            (vector-or-list? v) (apply-field-quoting v opts)
            (map-no-sql-type? v) (apply-sql-map-fn v #(apply-field-quoting % opts))
            :else (keyword->quoted-raw v opts))))
       vec))

(defn- apply-table-aliases [input-sql]
  (cond
    (some #{:from} [*clause*])
    (map-every-nth alias-table 1 input-sql)
    (some #{:join :left-join :right-join} [*clause*])
    (map-every-nth alias-table 2 input-sql)
    (some #{:insert-into} [*clause*])
    (if (coll? input-sql) input-sql (vector input-sql))
    :else input-sql))

(defn- apply-sql-value-corrections [input-sql]
  (cond
    (some #{:insert-into} [*clause*])
    (first input-sql)
    :else input-sql))

(defn sql-quote-string [field] (format "'%s'" field))

(defn apply-sql-formatting [input-sql opts]
  (-> input-sql
      apply-table-aliases
      apply-custom-function-logic
      (apply-field-quoting opts)
      apply-sql-call-to-all-functions
      apply-sql-inline
      apply-sql-value-corrections))

(defn- vectorize-sql [sql-map]
  (transform [SQLWalker list?] vec sql-map))

(defn sql-format-enhanced
  "HoneySQL input (map) drop-in replacement.

  opts:
  - rename-table-func - used to rename a table when the table/alias used as input is how user identifies.
  "
  [sql-map opts]
  (-> sql-map
      remove-map-nils
      vectorize-sql
      (apply-sql-map-fn #(apply-sql-formatting % opts))
      sql-format))

;(def approved-func
;  {:%greatest :greatest
;   :%least    :least})
; optional conditional (get approved-func i)

(defn insert-prepared-statement
  [table columns opts]
  (let [sql-columns (vec (map keyword columns))]
    (-> (hh/insert-into [(keyword table)])
        (sql-apply hh/columns sql-columns)
        ; Dummy values to generate query properly - excluded from return
        (hh/values [(repeat (count columns) "x")])
        (sql-format-enhanced opts)
        first
        vector)))

(defn value [v] (Value. v))

(defn- sql-inline-type? [v]
  (some #(% v) [string? number? boolean?]))

(defn ->sql-inline
  "Attempt removing paramters in favor of inline. Use when SQL parameterization isn't an option."
  [input-sql]
  (transform [SQLWalker sql-inline-type?]
             #(sql/inline
               (if (string? %) (str "'" % "'") %))
             input-sql))

(defn- type-map->type-check-fn [type-map]
  (fn [v] (some #(% v) (keys type-map))))

(defn- type-map->pre-process-fn [type-map]
  (fn [v]
    ((some
      (fn [[type-fn? pf]]
        (when (type-fn? v) (or pf (constantly v)))) type-map)
     v)))

(defn ->conform-strict
  "Supports SQL providers that demand strict types, e.g. Postgres and ignores honeysql default
   overrides.

   Strict-map is a map where the keys represent type checks that should be made and values
   represent (optional) pre-processors. E.g.

   {boolean? nil
   any-date-time-type? type->sql-date-time}

   In the above example, we want to ensure boolean to be kept as a value (not be changed to the
   default honeysql string). We want the same with our custom any-date-time-type? check but prior
   to making this a value, we want to change it using type->sql-date-time (perhaps to change it to
   SQL date time type.)
   "
  [input-sql strict-map]
  (let [strict-type? (type-map->type-check-fn strict-map)
        ->value (comp value (type-map->pre-process-fn strict-map))]
    (transform [SQLWalker strict-type?] ->value input-sql)))

(defn ->conform-tolerant
  "Similar to ->conform-strict but doesn't result in the value type.

   Traverses through SQL looking for matching type and applies fn.

   Useful for changing boolean type to integer, etc.

   {boolean? #(if (true? %) 1 0)}
   "
  [input-sql tolerant-map]
  (let [tolerant-type? (type-map->type-check-fn tolerant-map)
        ->type-fn (type-map->pre-process-fn tolerant-map)]
    (transform [SQLWalker tolerant-type?] ->type-fn input-sql)))