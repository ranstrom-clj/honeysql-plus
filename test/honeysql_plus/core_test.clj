(ns honeysql-plus.core-test
  (:require [clojure.test :refer [deftest is testing]]
            [honeysql.core :as sql]
            [honeysql.helpers :as hh]
            [honeysql-plus.core :as hh+]
            [clojure.string :as string]))

(deftest test-parse-sql-function
  (let [f hh+/parse-sql-function]
    (is (= (f :%%sys.dbms_crypto.hash) [[:%sys.dbms_crypto.hash]]))
    (is (= (f :%min.id) [[:%min] :id]))
    (is (= (f :%min.%max.t.field) [[:%min :%max] :t.field]))
    (is (= (f :%min.%max.field) [[:%min :%max] :field]))
    (is (= (f :%count.*) [[:%count] :*]))
    (is (= (f :%current_time) [[:%current_time] nil]))
    (is (= (f :field) [[] :field]))
    (is (= (f :field.field) [[] :field.field]))))

(deftest test-apply-custom-function-logic
  (binding [hh+/*top-level* true
            hh+/*clause* :select]
    (let [f (fn [v]
              (-> v
                  hh+/apply-custom-function-logic
                  hh+/apply-sql-call-to-all-functions
                  (#(apply hh/select %))
                  sql/format first))]
      (is (= (f [[:%min.%max.field :f1]]) "SELECT min(max(field)) AS f1"))
      (is (= (f [:%min.id]) "SELECT min(id)"))
      (is (= (f [[[:%lower [:%to_char [:%rawtohex [:%%sys.dbms_crypto.hash [:%%utl_raw.cast_to_raw :field1] 3]]]] :f1]])
             "SELECT lower(to_char(rawtohex(sys.dbms_crypto.hash(utl_raw.cast_to_raw(field1), ?)))) AS f1"))
      (is (= (f [:%count.*]) "SELECT count(*)"))
      (is (= (f [:%min.%max.t.field]) "SELECT min(max(t.field))"))
      (is (= (f [:t.field]) "SELECT t.field"))
      (is (= (f [[:t.field :f1]]) "SELECT t.field AS f1")))))

(deftest test-apply-custom-function-nesting
  (testing "apply-custom-function-nesting"
    (is (= (hh+/apply-custom-function-logic [[:%upper.%greatest [:col1 [:col2 :c]]]])
           [[:%upper [:%greatest [:col1 [:col2 :c]]]]]))))

(deftest test-apply-custom-functions
  (testing "apply-custom-functions"
    (is (= (hh+/apply-sql-call-to-all-functions [[:%upper [:%greatest [:col1 [:col2 :c]]]]])
           [(apply sql/call [:upper (apply sql/call [:greatest [:col1 [:col2 :c]]])])]))))

(deftest test-complete-formatting-examples
  (testing "Advanced formatting examples"
    (binding [hh+/*clause* :select hh+/*top-level* true]
      (let [d (-> [[:%rawtohex [:%%sys.dbms_crypto.hash [:%%utl_raw.cast_to_raw :field1] 3]]]
                  hh+/apply-custom-function-logic
                  hh+/apply-sql-call-to-all-functions)]
        (is (= [(apply sql/call [:rawtohex
                                 (apply sql/call [:sys.dbms_crypto.hash
                                                  (apply sql/call [:utl_raw.cast_to_raw :field1]) 3])])]
               d))))))

(deftest test-apply-sql-inline
  (testing "sql inline"
    (is (= (hh+/apply-sql-inline
            [:a [:b 23] :c "1" :d 2
             :e (sql/inline "a") (sql/inline 1)
             (sql/inline [:h 22 [:i "vv" :j 32 :k (hh+/value 36)]])])
           [:a [:b (sql/inline 23)] :c "1" :d (sql/inline 2)
            :e (sql/inline "a") (sql/inline 1)
            (sql/inline [:h (sql/inline 22) [:i "vv" :j (sql/inline 32) :k (hh+/value 36)]])]))))

(deftest test-sql-format-quoting-field
  (testing "sql-format-quoting-field"
    (let [m {:quoting :ansi}]
      (is (= (hh+/sql-format-quoting-field "_field" m) "_field"))
      (is (= (hh+/sql-format-quoting-field "field" m) "field"))
      (is (= (hh+/sql-format-quoting-field :field m) "\"field\""))
      (is (= (hh+/sql-format-quoting-field :_field m) "\"_field\"")))
    (let [m {:quoting :ansi :quote-when-fn #(string/starts-with? (name %) "_")}]
      (is (= (hh+/sql-format-quoting-field :field m) :field))
      (is (= (hh+/sql-format-quoting-field :_field m) "\"_field\""))
      (is (= (hh+/sql-format-quoting-field :dbo._field m) "dbo.\"_field\""))
      (is (= (hh+/sql-format-quoting-field :_dbo._field m) "\"_dbo\".\"_field\"")))))

(deftest test-insert-prepared-statement
  (testing "insert-prepared-statement"
    (is (= (hh+/insert-prepared-statement :dbo.table [:col1 :col2] {:quoting :sqlserver})
           ["INSERT INTO [dbo].[table] ([col1], [col2]) VALUES (?, ?)"]))))

(deftest test-sql-inline
  (testing "->sql-inline"
    (is (= (hh+/->sql-inline {:select [["v1" :col1] :col2 [:v2 :col3] [123 :col4]]})
           {:select [[(sql/inline "'v1'") :col1] :col2 [:v2 :col3] [(sql/inline 123) :col4]]}))))

(deftest test-conform-strict
  (testing "-conform-strict"
    (is (= (hh+/->conform-strict
            {:select [["v1" :col1] [true :col2] [123 :col3]]}
            {boolean? nil
             string?  #(str "'" % "'")})
           {:select [[(hh+/value "'v1'") :col1] [(hh+/value true) :col2] [123 :col3]]}))))

(deftest test-conform-tolerant
  (testing "-conform-tolerant"
    (let [v1 {:select [["v1" :col1] [true :col2] [false :col3] [123 :col4]]}]
      (is (= (hh+/->conform-tolerant v1 {boolean? #(if (true? %) 1 0)})
             {:select [["v1" :col1] [1 :col2] [0 :col3] [123 :col4]]}))
      (is (= (hh+/->conform-tolerant v1 {boolean? nil})
             {:select [["v1" :col1] [true :col2] [false :col3] [123 :col4]]})))))
