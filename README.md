# HoneySQL+

**YOU MAY BE LOST! This repository has some custom enhancements to [HoneySQL](https://github.com/jkk/honeysql).**

**[Go here to find the HoneySQL repository](https://github.com/jkk/honeysql)**

## Enhancements
Custom enhancements for HoneySQL.

### Function-chaining

```
:%upper.%lower.tbl.my_col -> upper(lower(tbl.my_col))
```

### Aliased functions

```
[:%%sys.dbms_crypto.hash [:%%utl_raw.cast_to_raw :my_col] 3]
->
sys.dbms_crypto.hash(utl_raw.cast_to_raw(my_col), 3))
```

### Selective Quoting

Quoting of fields is selective by default.

### Cleanup/conform

```
; Missing aliases added by default
[:table] -> [[:table :table]]
[[:table :t]] -> [[:table :t]]
```

## opts

Options passed as map:

```
{
 :rename-fn <fn>
 :quote-when-fn <fn>
 :quoting :keyword
}
```

## Disclaimer

**THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.**
